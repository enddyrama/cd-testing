
import React, { useState, useEffect } from 'react';
import {
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    TouchableOpacity,
    useColorScheme,
    View,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { SearchBar } from 'react-native-elements';
import HomeCard from '../component/HomeCard';
import axios from 'axios';

const homeScreen = (props) => {
    const urlPop = "https://api.themoviedb.org/3/movie/popular?api_key=1e38bb29c52806c0d87d4319f69486a9&language=en-US&page=1"
    const urlTren = "https://api.themoviedb.org/3/trending/all/day?api_key=1e38bb29c52806c0d87d4319f69486a9"
    const [popular, setPopular] = useState([]);
    const [trending, setTrending] = useState([]);
    const [loading, setLoading] = useState(true);

    const fetchData = () => {
        const getPopular = axios.get(urlPop)
        const getTrending = axios.get(urlTren)
        axios.all([getPopular, getTrending]).then(axios.spread(function (res1, res2) {
            setPopular(res1.data.results);
            setTrending(res2.data.results);
        }));

    }
    useEffect(() => {
        fetchData()
    }, [])

    const handleDetail = (item)=>{
        props.navigation.navigate('Detail', {item})
    }

    return (
        <ScrollView>
            <View style={{ backgroundColor: 'black' }}>
                <SearchBar placeholder="Search movies" round='true' containerStyle={{ backgroundColor: 'black' }} inputContainerStyle={{ backgroundColor: 'white' }} />
                <View style={{ padding: 5, justifyContent: 'space-between', flexDirection: 'row', }}>
                    <Text style={{ color: 'white' }}>Best Genre</Text>
                    <TouchableOpacity><Text style={{ color: 'white' }}>more</Text></TouchableOpacity>
                </View>
                <View style={{ justifyContent: 'space-around', flexDirection: 'row', padding: 10, flexWrap: 'wrap', display: 'flex' }}>
                    <TouchableOpacity style={styles.genre}>
                        <Icon name="rocket" size={20} color="black" />
                        <Text>Action</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.genre}>
                        <Icon name="rocket" size={20} color="black" />
                        <Text>Romance</Text>
                    </TouchableOpacity >
                    <TouchableOpacity style={styles.genre}>
                        <Icon name="rocket" size={20} color="black" />
                        <Text>Thriller</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.genre}>
                        <Icon name="rocket" size={20} color="black" />
                        <Text>Comedy</Text>
                    </TouchableOpacity>
                </View>
                <Text style={{ color: 'white', padding: 10 }}>Hot Movies</Text>
                
                    {popular.map((item, index) => (
                        <TouchableOpacity onPress={()=>handleDetail()}>
                        <HomeCard
                            key={index}
                            title={item.title}
                            overview={item.overview}
                            release={item.release_date}
                            poster={item.poster_path}
                        />
                        </TouchableOpacity>
                    ))}
                
            </View>
        </ScrollView>
    )
};

export default homeScreen

const styles = StyleSheet.create({
    genre: {
        backgroundColor: 'white',
        flexDirection: 'row',
        borderRadius: 100 / 2,
        padding: 5,
    }
})