import React from 'react'
import { StyleSheet, Text, View, TouchableWithoutFeedback } from 'react-native'

const ReviewCard = () => {
    return (

            <View style={styles.reviewCard}>
                
                <Text style={{fontSize: 20, fontWeight: 'bold', marginBottom: 10}}>Ass</Text>
                <Text style={{marginBottom: 10}}>Ass</Text>
                <Text numberOfLines={5} style={{}}>Ass</Text>

                <TouchableWithoutFeedback>
                    <Text style={styles.btn}>X</Text>
                </TouchableWithoutFeedback>

            </View>
    )
}

export default ReviewCard

const styles = StyleSheet.create({
    reviewCard: {
        backgroundColor: 'white',
        padding: 15,
        marginBottom: 15,
        elevation: 10,
        borderRadius: 8
    },
    btn: {
        position: 'absolute',
        right: 10,
        top: 10
    }
})
