import { NavigationContainer } from '@react-navigation/native'
import React from 'react'
import { StyleSheet, Text, View, TouchableWithoutFeedback, Image, TouchableOpacity } from 'react-native'

const HomeCard = (props) => {
    
    return (
        <View>
                <View style={styles.homeCard}>
                    <Image style={{ height: 300, width: 200, resizeMode: 'cover' }} source={{ uri: "https://image.tmdb.org/t/p/w500" + props.poster_path }} />
                    <Text style={{ fontSize: 20, fontWeight: 'bold', marginBottom: 10 }}>{props.title}</Text>
                    <Text style={{ marginBottom: 10 }}>{props.overview}</Text>

                    <TouchableWithoutFeedback>
                        <Text style={styles.btn}>X</Text>
                    </TouchableWithoutFeedback>

                </View>
        </View>
    )
}

export default HomeCard

const styles = StyleSheet.create({
    homeCard: {
        backgroundColor: 'white',
        padding: 15,
        marginBottom: 15,
        elevation: 10,
        borderRadius: 8
    },
    btn: {
        position: 'absolute',
        right: 10,
        top: 10
    }
})
